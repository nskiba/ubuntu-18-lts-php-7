FROM ubuntu:bionic
ENV TZ=America/Detroit
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -yqq \
    && apt-get install -yqq \
        php \
        php-pear \
        php-dev \
        git \
        php-apcu \
        php-gd \
        php-mysql \
        php-ldap \
        php-curl \
        libssh2-1-dev \
        php-ssh2 \
        imagemagick \
        php-imagick \
        php-xsl \
        php-intl \
        curl \
        sendmail \
    && pecl install scrypt \
    && echo "extension=scrypt.so" \
        > /etc/php/7.2/mods-available/scrypt.ini \
    && phpenmod scrypt \
    && phpenmod ssh2 \
    && curl -sS https://getcomposer.org/installer | php